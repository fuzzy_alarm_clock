/*
 * fuzzy_alarm_clock arduino sketch
 * copyright (C) 2011 Elena Grandi, Diego Roversi
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include <Wire.h>

#include <RealTimeClockDS1307.h>

// minutes of "dawn" before alarm
#define TIN 30
// "dawn" + "daylight"
#define TDAY 40
// "dawn" + "daylight" + blue blinding light
#define TOUT 60

// number of available alarms; max 10 for storage in the DS1307 ram 
#define NALARMS 4

// pins and addressed
#define RPIN 5
#define YPIN 6
#define BPIN 9

#define APIN0 3
#define APIN1 11

#define IINT 0
#define IPIN 2 

int st = 0; // alarm status (minutes from alarm - TIN)
char alarms[NALARMS][5]; // alarm settings
char cmin; // current minute
signed char a = -1; // current alarm
bool dbg = false; // print debug informations
bool ringing = false; // sound the alarm
bool pressed = false; // stop button status

#define NFREQ 9
int freq[] = { 255, 192, 128, 96, 64, 96, 128, 192, 255 }; // frequencies for the alarm

void setup () {
    Serial.begin(9600);
    Wire.begin();

    pinMode(RPIN,OUTPUT);
    pinMode(YPIN,OUTPUT);
    pinMode(BPIN,OUTPUT);

    pinMode(APIN0,INPUT);
    pinMode(APIN1,INPUT);
    TCCR2A = _BV(COM2A0) | _BV(COM2B1) | _BV(WGM20);
    TCCR2B = _BV(WGM22) | _BV(CS22);

    attachInterrupt(IINT, button_pressed, HIGH);

    analogWrite(RPIN,128);
    digitalWrite(YPIN,LOW);
    digitalWrite(BPIN,LOW);
   
    // if the RTC is already running read alarms and status, 
    // otherwise set everything to a sane default
    if ( RTC.readData(0x00) >> 7 ) {
        for ( int i = 0 ; i < NALARMS ; i ++ ) {
            for ( int j = 0; j < 5 ; j ++ ) {
                alarms[i][j] = 0;
            }
        }
        st = 0;
        a = -1;
        save_status();
    } else {
        st = RTC.readData(0x08);
        a = RTC.readData(0x09);
        cmin = RTC.readData(0x0a);
        // This only works if the arduino had no power for a 
        // "short" time. This is by design. :D
        if ( a >= 0 ) {
            RTC.readClock();
            st = st + (RTC.getMinutes() - cmin) % 60;
            cmin = RTC.getMinutes();
            save_status();
        }
        for ( int i = 0; i < NALARMS ; i ++ ) {
            for ( int j = 0; j < 5 ; j ++ ) {
                alarms[i][j] = RTC.readData(0x0b + i*5 + j);
            }
        }
    }
    reset_leds();
}

void loop () {
  
  // read commands from serial
  check_serial();
  
  // read time, check alarms
  check_time();

  if ( dbg ) {
      s_print_time();
      Serial.print("st: ");
      Serial.print(st,DEC);
      Serial.print(", a: ");
      Serial.print(a,DEC);
      Serial.print(", cmin: ");
      Serial.print(cmin,DEC);
      Serial.print(", ringing: ");
      Serial.print(ringing);
      Serial.print(", pressed: ");
      Serial.println(pressed);
  }
  
  // act on status: LEDs and buzzer
  if ( st > 0 ) {
      set_leds();
  }
  if ( st == TIN + 1) {
      if ( RTC.getSeconds() < 5 ) {
          ringing = true;
      }
      if ( pressed ) {
          ringing = false;
      }
  } else {
      ringing = false;
      pressed = false;
  }
  if ( ringing ) {
      ring_alarm();
  }
  // wait about till the next second

  delay(1000);
  
}

void save_status() {
    RTC.writeData(0x08,st);
    RTC.writeData(0x09,a);
    RTC.writeData(0x0a,cmin);
}

int has_alarm() {
    if ( RTC.readData(0x00) >> 7 ) {
        return false;
    }
    for ( int i = 0; i < NALARMS ; i ++ ) {
        // we can skip checking the hour/minute field
        for ( int j = 0; j < 3 ; j ++ ) {
            if ( alarms[i][j] != 0 ) {
                return true;
            }
        }
    }
    return false;
}

// ****** Serial interface management *************************************** //

void check_serial() {
    int rec = Serial.read();
    switch (rec) {
        case 'a':
            s_set_alarm();
            break;
        case 's':
            s_set_time();
            break;
        case 'p':
            s_print_alarms();
            break;
        case 't':
            s_print_time();
            break;
        case 'd':
            s_toggle_debug();
            break;
        case 'D':
            s_dump_rtc();
            break;
        case 'l':
            s_led_test();
            break;
        case 'h':
            s_print_help();
            break;
        case 'r':
            s_reset_alarms();
            break;
    }
}

void s_set_alarm() {
    int i = s_read_dig();
    for ( int j = 0; j < 5 ; j++) {
        alarms[i][j] = s_read_2hex();
    }
    for ( int j = 0; j < 5 ; j++ ) {
        RTC.writeData(0x0b + i*5 + j,alarms[i][j]);
    }
    Serial.print("Alarm ");
    Serial.print(i,DEC);
    Serial.println(" set.");
    reset_leds();
}

void s_set_time() {
    RTC.start();
    RTC.setYear(s_read_2dig());
    RTC.setMonth(s_read_2dig());
    RTC.setDate(s_read_2dig());
    RTC.setDayOfWeek(s_read_dig());
    RTC.setHours(s_read_2dig());
    RTC.setMinutes(s_read_2dig());
    RTC.setSeconds(s_read_2dig());
    RTC.setClock();
    Serial.print("Time set: ");
    s_print_time();
    reset_leds();
}

int s_read_dig() {
    int rec;
    rec = Serial.read();
    while ( rec == -1 ) {
        rec = Serial.read();
    }
    return rec - 48;
}

int s_read_2dig() {
    int n;
    n = s_read_dig() * 10;
    n = n + s_read_dig();
    return n;
}

int s_read_hex() {
    int rec;
    rec = Serial.read();
    while ( rec == -1 ) {
        rec = Serial.read();
    }
    if ( rec >= 48 && rec < 58 ) {
        return rec - 48;
    } else if ( rec >= 65 && rec < 71 ) {
        return rec - 55;
    } else if ( rec >= 97 && rec < 103 ) {
        return rec - 87;
    } else {
        return 0;
    }
}

int s_read_2hex() {
    int n;
    n = s_read_hex() * 16;
    n = n + s_read_hex();
    return n;
}

void s_print_alarms() {
    for ( int i = 0; i < NALARMS ; i++) {
        Serial.print(i,DEC);
        Serial.print(" - ");
        for ( int j = 0; j < 5 ; j++) {
            Serial.print(alarms[i][j],DEC);
            Serial.print(" ");
        }
        Serial.println("");
    }
}

void s_print_time() {
    RTC.readClock();
    Serial.print(RTC.getYear(),DEC);
    Serial.print("/");
    Serial.print(RTC.getMonth(),DEC);
    Serial.print("/");
    Serial.print(RTC.getDate(),DEC);
    Serial.print(" (");
    Serial.print(RTC.getDayOfWeek(),DEC);
    Serial.print(") ");
    Serial.print(RTC.getHours(),DEC);
    Serial.print(":");
    Serial.print(RTC.getMinutes(),DEC);
    Serial.print(":");
    Serial.println(RTC.getSeconds(),DEC);
}

void s_toggle_debug() {
    if ( dbg ) {
        dbg = false;
    } else {
        dbg = true;
    }
}

void s_dump_rtc() {
    Serial.print("| 0x00 | ");
    Serial.print(RTC.readData(0x00),BIN); 
    Serial.println(" |  |");
    Serial.print("| 0x08 | ");
    Serial.print(RTC.readData(0x08),BIN);
    Serial.println(" | st |");
    Serial.print("| 0x09 | ");
    Serial.print(RTC.readData(0x09),BIN);
    Serial.println(" | a |");
    Serial.print("| 0x0a | ");
    Serial.print(RTC.readData(0x0a),BIN);
    Serial.println(" | cmin |");
    for (int i = 0; i < NALARMS + 3 ; i ++ ) {
        for ( int j=0; j < 5 ; j ++ ) {
            Serial.print("| 0x");
            Serial.print(i,HEX);
            Serial.print(" | ");
            Serial.print(RTC.readData(0x0b + i*5 + j),BIN);
            Serial.print(" | Alarm ");
            Serial.print(i,DEC);
            Serial.println(" |");
        }
    }
}

void s_led_test() {
    if ( a < 0 ) {
        Serial.println("Testing LEDs");
        digitalWrite(RPIN,HIGH);
        digitalWrite(YPIN,HIGH);
        digitalWrite(BPIN,HIGH);
        delay(1000);
        reset_leds();
    }
}

void s_reset_alarms() {
    for ( int i = 0 ; i < NALARMS ; i ++ ) {
        for ( int j = 0; j < 5 ; j ++ ) {
            alarms[i][j] = 0;
        }
    }
    st = 0;
    a = -1;
    save_status();
    reset_leds();
}

void s_print_help() {
    Serial.println("");
    Serial.println("  a<s> - set an alarm");
    Serial.println("         <s> is nhhhhhhhhhh");
    Serial.println("         alarm ID");
    Serial.println("         one bit (in hex) for day of week");
    Serial.println("         hex values for month, day");
    Serial.println("         hex values for hour, minutes");
    Serial.println("         (relative to start-of-dawn time)");
    Serial.println("  s<s> - set the clock");
    Serial.println("         <s> is yymmgguHHMMSS");
    Serial.println("  p    - print the alarms");
    Serial.println("  t    - print the clock");
    Serial.println("  d    - toggle printing of debug informations");
    Serial.println("  D    - dump RTC registers (interesting) data");
    Serial.println("  r    - reset status and alarms");
    Serial.println("  l    - test LEDs");
    Serial.println("  h    - print this help");
}

void button_pressed() {
    pressed = true;
}

// ****** Time management *************************************************** //

// Set the current time
void set_time(int y,int m,int d, int w, int hh, int mm, int ss) {
    RTC.setYear(y);
    RTC.setMonth(m);
    RTC.setDate(m);
    RTC.setDayOfWeek(w);
    RTC.setHours(hh);
    RTC.setMinutes(mm);
    RTC.setSeconds(ss);
    RTC.switchTo24h();
    RTC.setClock();
}

void check_time() {
    RTC.readClock();

    int mm = RTC.getMinutes();
    int hour = RTC.getHours();
    int wday = RTC.getDayOfWeek();
    int day = RTC.getDate();
    int month = RTC.getMonth();

    if ( a < 0 ) {
        for ( int i = 0; i < NALARMS ; i ++ ) {
            // check alarm i
            if ( ( alarms[i][0] & ( 1 << (wday - 1) ) ) || 
                    (month == alarms[i][1] && day == alarms[i][2]) ) {
                // this is alarm day!
                if ( hour == alarms[i][3] && mm == alarms[i][4]) {
                    // this is alarm hour!
                    a = i;
                    st = 1;
                    cmin = mm;
                    save_status();
                    if ( ( alarms[i][0] & 128 ) == 0 ) {
                        // this alarm won't be repeated
                        alarms[i] = { 0,0,0,0,0 };
                        for ( int j = 0; j < 5 ; j++ ) {
                            RTC.writeData(0x0b + i*5 + j,0);
                        }
                    }
                    break;
                }
            } 
        }
    } else {
        if ( cmin != mm ) {
            cmin = mm;
            st++;
            save_status();
        } 
    }

}

// ****** LED management **************************************************** //

void set_leds() {
  if ( st > 0 && st <= TIN) {
      int y = int(float(st*255)/(TIN+1));
      int r = 255 - y;
      analogWrite(RPIN,r);
      analogWrite(YPIN,y);
  } else if ( st > TIN && st <= TDAY ) {
      analogWrite(RPIN,0);
      analogWrite(YPIN,255);
      analogWrite(BPIN,0);
  } else if ( st > TDAY && st <= TOUT ) {
      analogWrite(RPIN,0);
      analogWrite(YPIN,0);
      analogWrite(BPIN,255);
  } else if ( st == TOUT + 1 ) {
      // reset stuff
      st = 0;
      a = -1;
      save_status();
      reset_leds();
  } 
}

void reset_leds() {
    if ( a < 0 ) {
        if ( has_alarm() ) {
            analogWrite(RPIN,128);
            analogWrite(YPIN,0);
            analogWrite(BPIN,0);
        } else {
            analogWrite(RPIN,0);
            analogWrite(YPIN,16);
            analogWrite(BPIN,16);
        }
    }
}

// PC speaker
//
void ring_alarm() {

    pinMode(APIN1,OUTPUT);
    for (int i=0; i<NFREQ ; i++) {
        OCR2A = freq[i];
        delay(50);
    }
    pinMode(APIN1,INPUT);

}

// vim: set filetype=c:
