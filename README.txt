===================
 Fuzzy Alarm Clock
===================

---------------------------------------------------
 by Elena ``of Valhalla'' Grandi and Diego Roversi
---------------------------------------------------

The Fuzzy Alarm Clock is a device that conveys just the temporal informations 
needed during the night, and nothing else.

There is no time/date display, but coloured LEDs are used to show an 
aproximation of the time until (and since) alarm time, and 
an old PC speaker works as the Sound Emitting Device.

Design notes and additional descriptions are available on the `web page
<http://www.trueelena.org/computers/projects/fuzzy_alarm_clock.html>`_.

Requirements
============

Arduino sketch
--------------

The arduino sketch ``fuzzy_alarm_clock_ds1307`` requires an old (<1.0) 
version of the arduino tools and can be built outside the IDE 
thanks to the SConstruct file from arscons_.
It also requires an old (pre 1.0 migration) version of the
RealTimeClockDS1307_ library by davidhbrown.

.. _arscons: http://code.google.com/p/arscons/
.. _RealTimeClockDS1307: https://github.com/davidhbrown/RealTimeClockDS1307

Conversion to arduino 1.0 is planned as soon as a compatible
SConstruct file will be available.

Schematics
----------

This repository includes the kicad project 

Beside the standard libraries, I've used the `Arduino Shield Modules for 
KiCad V3` by nicholasclewis; the needed component has been included by kicad 
in the ``fuzzy_alarm_clock_ds1307-cache.lib`` file.

.. _`Arduino Shield Modules for KiCad V3`: http://www.thingiverse.com/thing:9630

An `SVG rendering of the schematics`_ is available on the web page, 
but during active developement it could reflect an older version.

.. _`SVG rendering of the schematics`: http://www.trueelena.org/computers/projects/fuzzy_alarm_clock/fuzzy_alarm_clock_ds1307.svg

