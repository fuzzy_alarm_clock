EESchema Schematic File Version 2  date Mon 02 Jan 2012 21:02:29 CET
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:ds1307_pcf8583
LIBS:fuzzy_alarm_clock_ds1307-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "2 jan 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 9100 4400
Wire Wire Line
	7900 4400 9100 4400
Connection ~ 9100 4100
Wire Wire Line
	8300 4100 9100 4100
Connection ~ 9100 3600
Wire Wire Line
	8300 3600 9100 3600
Wire Wire Line
	9100 2400 9100 5900
Wire Wire Line
	9100 2400 6800 2400
Wire Wire Line
	6800 2400 6800 3100
Wire Wire Line
	6500 4100 6700 4100
Wire Wire Line
	6500 4000 7250 4000
Wire Wire Line
	6500 3600 6700 3600
Wire Wire Line
	7400 3200 7400 3400
Wire Wire Line
	2600 5100 2900 5100
Wire Wire Line
	1700 4700 1000 4700
Connection ~ 2800 4500
Wire Wire Line
	2800 4200 2800 4500
Connection ~ 3200 3600
Wire Wire Line
	3200 3700 3200 3600
Wire Wire Line
	1700 4500 1400 4500
Wire Wire Line
	1400 4500 1400 3600
Wire Wire Line
	1400 3600 4600 3600
Wire Wire Line
	2800 3700 2800 3600
Connection ~ 2800 3600
Wire Wire Line
	2600 4500 3000 4500
Wire Wire Line
	3000 4500 3000 4600
Wire Wire Line
	3000 4600 4600 4600
Wire Wire Line
	2600 4700 3200 4700
Wire Wire Line
	3200 4500 4600 4500
Wire Wire Line
	3200 4700 3200 4200
Connection ~ 3200 4500
Wire Wire Line
	1000 5300 1300 5300
Wire Wire Line
	1300 5300 1300 4900
Wire Wire Line
	1300 4900 1700 4900
Wire Wire Line
	6800 3100 6500 3100
Wire Wire Line
	1700 5100 1700 5900
Wire Wire Line
	1700 5900 9100 5900
Wire Wire Line
	3500 5100 3700 5100
Wire Wire Line
	3700 5100 3700 5900
Connection ~ 3700 5900
Wire Wire Line
	7400 3400 6500 3400
Wire Wire Line
	7600 3200 7800 3200
Wire Wire Line
	7800 3200 7800 3400
Wire Wire Line
	7200 3600 7900 3600
Wire Wire Line
	7750 4000 8400 4000
Wire Wire Line
	7200 4100 7900 4100
Wire Wire Line
	7800 3400 9100 3400
Connection ~ 9100 3400
Wire Wire Line
	8800 4000 9100 4000
Connection ~ 9100 4000
Wire Wire Line
	4350 3600 4350 5000
Wire Wire Line
	4350 5000 7000 5000
Connection ~ 4350 3600
Wire Wire Line
	7400 4400 6500 4400
Connection ~ 7000 4400
$Comp
L SW_PUSH SW?
U 1 1 4F020CFA
P 7000 4700
F 0 "SW?" H 7150 4810 50  0000 C CNN
F 1 "SW_PUSH" H 7000 4620 50  0000 C CNN
	1    7000 4700
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4F020CCF
P 7650 4400
F 0 "R?" V 7730 4400 50  0000 C CNN
F 1 "R" V 7650 4400 50  0000 C CNN
	1    7650 4400
	0    1    1    0   
$EndComp
$Comp
L BATTERY BT?
U 1 1 4EF0662F
P 3200 5100
F 0 "BT?" H 3200 5300 50  0000 C CNN
F 1 "BATTERY" H 3200 4910 50  0000 C CNN
	1    3200 5100
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4EF065A1
P 8100 4100
F 0 "D?" H 8100 4200 50  0000 C CNN
F 1 "LED R" H 8100 4000 50  0000 C CNN
	1    8100 4100
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4EF0659B
P 8600 4000
F 0 "D?" H 8600 4100 50  0000 C CNN
F 1 "LED Y" H 8600 3900 50  0000 C CNN
	1    8600 4000
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4EF06594
P 8100 3600
F 0 "D?" H 8100 3700 50  0000 C CNN
F 1 "LED B" H 8100 3500 50  0000 C CNN
	1    8100 3600
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 4EF06585
P 6950 4100
F 0 "R?" V 7030 4100 50  0000 C CNN
F 1 "220" V 6950 4100 50  0000 C CNN
	1    6950 4100
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4EF06573
P 7500 4000
F 0 "R?" V 7580 4000 50  0000 C CNN
F 1 "220" V 7500 4000 50  0000 C CNN
	1    7500 4000
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4EF0655A
P 6950 3600
F 0 "R?" V 7030 3600 50  0000 C CNN
F 1 "220" V 6950 3600 50  0000 C CNN
	1    6950 3600
	0    -1   -1   0   
$EndComp
$Comp
L CAPAPOL C?
U 1 1 4EF06517
P 7600 3400
F 0 "C?" H 7650 3500 50  0000 L CNN
F 1 "CAPAPOL" H 7650 3300 50  0000 L CNN
	1    7600 3400
	0    1    1    0   
$EndComp
$Comp
L SPEAKER SP?
U 1 1 4EF064D7
P 7500 2900
F 0 "SP?" H 7400 3150 70  0000 C CNN
F 1 "SPEAKER" H 7400 2650 70  0000 C CNN
	1    7500 2900
	0    -1   -1   0   
$EndComp
$Comp
L CRYSTAL X?
U 1 1 4EF06462
P 1000 5000
F 0 "X?" H 1000 5150 60  0000 C CNN
F 1 "CRYSTAL" H 1000 4850 60  0000 C CNN
	1    1000 5000
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4EF06349
P 2800 3950
F 0 "R?" V 2880 3950 50  0000 C CNN
F 1 "2k2" V 2800 3950 50  0000 C CNN
	1    2800 3950
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 4EF06341
P 3200 3950
F 0 "R?" V 3280 3950 50  0000 C CNN
F 1 "2k2" V 3200 3950 50  0000 C CNN
	1    3200 3950
	1    0    0    -1  
$EndComp
$Comp
L DS1307 IC?
U 1 1 4EF062D1
P 2100 4800
F 0 "IC?" H 1800 5250 50  0000 L BNN
F 1 "DS1307" H 1800 4300 50  0000 L BNN
F 2 "ds1307_pcf8583-DIL08" H 2100 4950 50  0001 C CNN
	1    2100 4800
	1    0    0    -1  
$EndComp
$Comp
L ARDUINO_SHIELD SHIELD?
U 1 1 4EF062B8
P 5550 3800
F 0 "SHIELD?" H 5200 4750 60  0000 C CNN
F 1 "ARDUINO_SHIELD" H 5600 2850 60  0000 C CNN
	1    5550 3800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
